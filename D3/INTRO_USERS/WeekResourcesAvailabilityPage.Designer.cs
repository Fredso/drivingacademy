﻿namespace INTRO_USERS
{
    partial class WeekResourcesAvailabilityPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerWeek = new System.Windows.Forms.DateTimePicker();
            this.listViewWeek = new System.Windows.Forms.ListView();
            this.listViewDay = new System.Windows.Forms.ListView();
            this.labelDate = new System.Windows.Forms.Label();
            this.comboBoxCarI1 = new System.Windows.Forms.ComboBox();
            this.comboBoxCarI2 = new System.Windows.Forms.ComboBox();
            this.comboBoxCarI3 = new System.Windows.Forms.ComboBox();
            this.comboBoxCarI4 = new System.Windows.Forms.ComboBox();
            this.buttonSaveCar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonCancelSchedule = new System.Windows.Forms.Button();
            this.buttonSaveSchedule = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dateTimePickerWeek
            // 
            this.dateTimePickerWeek.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerWeek.Location = new System.Drawing.Point(74, 29);
            this.dateTimePickerWeek.Name = "dateTimePickerWeek";
            this.dateTimePickerWeek.Size = new System.Drawing.Size(104, 20);
            this.dateTimePickerWeek.TabIndex = 0;
            this.dateTimePickerWeek.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // listViewWeek
            // 
            this.listViewWeek.Location = new System.Drawing.Point(12, 80);
            this.listViewWeek.MultiSelect = false;
            this.listViewWeek.Name = "listViewWeek";
            this.listViewWeek.Size = new System.Drawing.Size(358, 189);
            this.listViewWeek.TabIndex = 1;
            this.listViewWeek.UseCompatibleStateImageBehavior = false;
            this.listViewWeek.View = System.Windows.Forms.View.Details;
            this.listViewWeek.SelectedIndexChanged += new System.EventHandler(this.listViewWeek_SelectedIndexChanged);
            // 
            // listViewDay
            // 
            this.listViewDay.Location = new System.Drawing.Point(406, 47);
            this.listViewDay.MultiSelect = false;
            this.listViewDay.Name = "listViewDay";
            this.listViewDay.Size = new System.Drawing.Size(382, 273);
            this.listViewDay.TabIndex = 2;
            this.listViewDay.UseCompatibleStateImageBehavior = false;
            this.listViewDay.View = System.Windows.Forms.View.Details;
            this.listViewDay.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listViewDay_MouseDown);
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Location = new System.Drawing.Point(497, 9);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(30, 13);
            this.labelDate.TabIndex = 3;
            this.labelDate.Text = "Date";
            // 
            // comboBoxCarI1
            // 
            this.comboBoxCarI1.FormattingEnabled = true;
            this.comboBoxCarI1.Location = new System.Drawing.Point(84, 304);
            this.comboBoxCarI1.Name = "comboBoxCarI1";
            this.comboBoxCarI1.Size = new System.Drawing.Size(67, 21);
            this.comboBoxCarI1.TabIndex = 4;
            // 
            // comboBoxCarI2
            // 
            this.comboBoxCarI2.FormattingEnabled = true;
            this.comboBoxCarI2.Location = new System.Drawing.Point(157, 304);
            this.comboBoxCarI2.Name = "comboBoxCarI2";
            this.comboBoxCarI2.Size = new System.Drawing.Size(67, 21);
            this.comboBoxCarI2.TabIndex = 5;
            // 
            // comboBoxCarI3
            // 
            this.comboBoxCarI3.FormattingEnabled = true;
            this.comboBoxCarI3.Location = new System.Drawing.Point(230, 304);
            this.comboBoxCarI3.Name = "comboBoxCarI3";
            this.comboBoxCarI3.Size = new System.Drawing.Size(67, 21);
            this.comboBoxCarI3.TabIndex = 6;
            // 
            // comboBoxCarI4
            // 
            this.comboBoxCarI4.FormattingEnabled = true;
            this.comboBoxCarI4.Location = new System.Drawing.Point(303, 304);
            this.comboBoxCarI4.Name = "comboBoxCarI4";
            this.comboBoxCarI4.Size = new System.Drawing.Size(67, 21);
            this.comboBoxCarI4.TabIndex = 7;
            // 
            // buttonSaveCar
            // 
            this.buttonSaveCar.Location = new System.Drawing.Point(276, 331);
            this.buttonSaveCar.Name = "buttonSaveCar";
            this.buttonSaveCar.Size = new System.Drawing.Size(94, 34);
            this.buttonSaveCar.TabIndex = 8;
            this.buttonSaveCar.Text = "Assign Cars";
            this.buttonSaveCar.UseVisualStyleBackColor = true;
            this.buttonSaveCar.Click += new System.EventHandler(this.buttonSaveCar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(403, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Date Displayed:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Week of:";
            // 
            // buttonCancelSchedule
            // 
            this.buttonCancelSchedule.Location = new System.Drawing.Point(711, 331);
            this.buttonCancelSchedule.Name = "buttonCancelSchedule";
            this.buttonCancelSchedule.Size = new System.Drawing.Size(77, 34);
            this.buttonCancelSchedule.TabIndex = 15;
            this.buttonCancelSchedule.Text = "Cancel";
            this.buttonCancelSchedule.UseVisualStyleBackColor = true;
            this.buttonCancelSchedule.Click += new System.EventHandler(this.buttonCancelSchedule_Click);
            // 
            // buttonSaveSchedule
            // 
            this.buttonSaveSchedule.Location = new System.Drawing.Point(611, 331);
            this.buttonSaveSchedule.Name = "buttonSaveSchedule";
            this.buttonSaveSchedule.Size = new System.Drawing.Size(94, 34);
            this.buttonSaveSchedule.TabIndex = 14;
            this.buttonSaveSchedule.Text = "Save Schedule";
            this.buttonSaveSchedule.UseVisualStyleBackColor = true;
            this.buttonSaveSchedule.Click += new System.EventHandler(this.buttonSaveSchedule_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 307);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Cars Asigned:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(376, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Please select a date of the first column to see the detail of the day on the righ" +
    "t:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(279, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Please select a date to display the week on the left panel:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(81, 288);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Instructor 1:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(157, 288);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Instructor 2:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(233, 288);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Instructor 3:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(303, 288);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Instructor 4:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(403, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(318, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Click below in the values to assign/remove slots to the Instructors:";
            // 
            // WeekResourcesAvailabilityPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 377);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancelSchedule);
            this.Controls.Add(this.buttonSaveSchedule);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonSaveCar);
            this.Controls.Add(this.comboBoxCarI4);
            this.Controls.Add(this.comboBoxCarI3);
            this.Controls.Add(this.comboBoxCarI2);
            this.Controls.Add(this.comboBoxCarI1);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.listViewDay);
            this.Controls.Add(this.listViewWeek);
            this.Controls.Add(this.dateTimePickerWeek);
            this.Name = "WeekResourcesAvailabilityPage";
            this.Text = "Week Resources Page";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerWeek;
        private System.Windows.Forms.ListView listViewWeek;
        private System.Windows.Forms.ListView listViewDay;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.ComboBox comboBoxCarI1;
        private System.Windows.Forms.ComboBox comboBoxCarI2;
        private System.Windows.Forms.ComboBox comboBoxCarI3;
        private System.Windows.Forms.ComboBox comboBoxCarI4;
        private System.Windows.Forms.Button buttonSaveCar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonCancelSchedule;
        private System.Windows.Forms.Button buttonSaveSchedule;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}