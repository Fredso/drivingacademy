﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class InstructorsSchedulePage : Form
    {
        public InstructorsSchedulePage(string userName, string userType)
        {
            InitializeComponent();
            //It activates the form user combobox depending on the user role
            switch (userType)
            {
                case "Admins":
                    SQL.editComboBoxItems(comboBoxInstructor, "SELECT username FROM Instructors");
                    comboBoxInstructor.Enabled = true;
                    break;
                case "Instructors":
                    comboBoxInstructor.Text = userName;
                    comboBoxInstructor.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        private void comboBoxInstructor_SelectedValueChanged(object sender, EventArgs e)
        {
            //It removes the checkboxes from the Form
            removeCheckBoxes();
            //It searches in the slots table all the hours available for the selected date and it creates a checkbox for each hour
            obtainDayTimes(dateTimePickerSchedule.Value);
            //It obtains the schedule from the Instructor for that date and actualizes the checkboxes
            obtainSchedule(dateTimePickerSchedule.Value, comboBoxInstructor.Text);
            //It actualizes the number of hours scheduled for the day
            labelDayHours.Text = calculateDayHours().ToString() + " hours";
        }

        //It changes the back color if the control to white
        private void comboBoxInstructor_Click(object sender, EventArgs e)
        {
            comboBoxInstructor.BackColor = Color.White;
        }

        //It actualizes the checkboxes when selecting a new date
        private void dateTimePickerSchedule_ValueChanged(object sender, EventArgs e)
        {
            //It removes the checkboxes from the Form
            removeCheckBoxes();
            //It searches in the slots table all the hours available for the selected date and it creates a checkbox for each hour
            obtainDayTimes(dateTimePickerSchedule.Value);
            //It obtains the schedule from the Instructor for that date and actualizes the checkboxes
            obtainSchedule(dateTimePickerSchedule.Value, comboBoxInstructor.Text);
            //It actualizes the number of hours scheduled for the day
            labelDayHours.Text = calculateDayHours().ToString() + " hours";
        }

        //It saves the schedule data in the DB
        private void buttonSaveSchedule_Click(object sender, EventArgs e)
        {
            //It checks that the text and combo boxes have something typed in them
            bool hasText = ControlFunctions.checkTextControls(this);
            if (!hasText)
            {
                MessageBox.Show("Please make sure all fields have data.");
                comboBoxInstructor.Focus();
                return;
            }
            foreach (Control c in Controls)
            {
                //if its a checkbox
                if (c is CheckBox)
                {
                    if ((c as CheckBox).Checked == true)
                    {
                        //It checks if the data already exists not to duplicate
                        if (!checkScheduleInDB(comboBoxInstructor.Text, 1, dateTimePickerSchedule.Value, DateTime.Parse(c.Text)))
                        {
                            insertInstructorSchedule(comboBoxInstructor.Text, 1, dateTimePickerSchedule.Value, DateTime.Parse(c.Text));
                        }
                    }
                    else
                    {
                        //It removes the schedule if it exists
                        if (checkScheduleInDB(comboBoxInstructor.Text, 1, dateTimePickerSchedule.Value, DateTime.Parse(c.Text)))
                        {
                            deleteInstructorSchedule(comboBoxInstructor.Text, 1, dateTimePickerSchedule.Value, DateTime.Parse(c.Text));
                        }
                    }
                }
            }
        }

        //It checks the existing slots for a instructor in a date and time
        private Boolean checkScheduleInDB(string instructor, int slotId, DateTime myDate, DateTime myTime)
        {
            string slotTimeQuery = "SELECT slotTime FROM Appointments WHERE usernameInstructor= '" + instructor + "' AND slotDate= '" + ControlFunctions.formatToSQLDate(myDate) + "' AND slotTime= '" + myTime + "'";
            //string slotTimeQuery = "SELECT slotTime FROM Appointments WHERE slotDate= '" + formatToSQLDate(myDate) + "' AND slotTime= '" + myTime + "' AND id IN (SELECT idAppointments FROM Assignments WHERE usernameInstructor= '" + instructor + "')";
            //It gets data from database
            SQL.selectQuery(slotTimeQuery);
            //It checks that there is something to write
            if (SQL.read.HasRows)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //It obtains the schedule for a instructor and a date and checks the corresponding checkboxes
        private void obtainSchedule(DateTime myDate, string instructor)
        {
            string slotTimesQuery = "SELECT slotTime FROM Appointments WHERE usernameInstructor= '" + instructor + "' AND slotDate= '" + ControlFunctions.formatToSQLDate(myDate) + "'";
            //string slotTimesQuery = "SELECT slotTime FROM Appointments WHERE slotDate= '" + formatToSQLDate(myDate) + "' AND id IN (SELECT idAppointments FROM Assignments WHERE usernameInstructor= '" + instructor + "')";
            //It gets data from database
            SQL.selectQuery(slotTimesQuery);
            //It checks that there is something to write
            if (SQL.read.HasRows)
            {
                short i = 0;
                while (SQL.read.Read())
                {
                    foreach (Control c in this.Controls)
                    {
                        if (c is CheckBox)
                        {
                            //It checks the existing slots
                            if (c.Text== SQL.read[0].ToString())
                            {
                                (c as CheckBox).Checked = true;
                            }
                        }
                    }
                    i++;
                }
            }
        }

        // It creates as many checkboxes as slot times available for the selected date
        private void obtainDayTimes(DateTime myDate)
        {
            //labelTest.Text = formatToSQLDate(myDate);
            string slotTimesQuery = "SELECT DISTINCT slotTime FROM TimeSlots WHERE slotDate= '" + ControlFunctions.formatToSQLDate(myDate) + "'";
            //It gets data from database
            SQL.selectQuery(slotTimesQuery);
            //It checks that there is something to write
            short i = 0;
            if (SQL.read.HasRows)
            {
                CheckBox box;
                while (SQL.read.Read())
                {
                    box = new CheckBox();
                    box.Name = "CB" + i;
                    box.Tag = i.ToString();
                    box.Text = SQL.read[0].ToString();
                    box.AutoSize = true;
                    box.Location = new Point(100, 60 + i * 20); //vertical
                    box.Click += (sender, e) =>
                    {
                        if(calculateDayHours()>8)
                        {
                            box.Checked = false;
                            MessageBox.Show("You can only select 8 hours per day");
                        }
                        labelDayHours.Text = calculateDayHours().ToString() + " hours";
                    };
                    Controls.Add(box);
                    i++;
                }
            }
            //label1.Text = i.ToString();
        }

        //It calculates the number of checkboxes clicked
        private short calculateDayHours()
        {
            short hoursNumber = 0;
            foreach (Control c in this.Controls)
            {
                if (c is CheckBox)
                {
                    //It checks the number of checkboxes checked
                    if ((c as CheckBox).Checked == true)
                    {
                        hoursNumber++;
                    }
                }
            }
            return hoursNumber;
        }

        //It returns the left characters from the string
        private string StrLeft(string str, int length)
        {
            return str.Substring(0, Math.Min(length, str.Length));
        }

        //It inserts the checked available times in the Appointments table as available
        private void insertInstructorSchedule(string instructor, int slotId, DateTime myDate, DateTime myTime)
        {
            //It inserts first the data in the Appointments Table
            try
            {
                var insertQuery = $"INSERT INTO Appointments (usernameInstructor, idTimeSlot, slotDate, slotTime) VALUES('{instructor}', {slotId}, '{ControlFunctions.formatToSQLDate(myDate)}', '{myTime}')";
                SQL.executeQuery(insertQuery);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
        }
        ////It inserts the checked available times in the Appointments table as available for the 2nd configuration
        //private void insertInstructorSchedule(string instructor, int slotId, DateTime myDate, DateTime myTime)
        //{
        //    //It inserts first the data in the Appointments Table
        //    try
        //    {
        //        var insertQuery = $"INSERT INTO Appointments (idTimeSlot, slotDate, slotTime) VALUES({slotId}, '{formatToSQLDate(myDate)}', '{myTime}')";
        //        SQL.executeQuery(insertQuery);
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
        //        return;
        //    }
        //    //It makes a query to obtain the Appointment id from the Appointment
        //    int AppointmentsId = obtainAppointmentId(slotId, myDate, myTime);
        //    if (AppointmentsId != 0)
        //    {
        //        //It inserts later the data in the Assignments Table
        //        try
        //        {
        //            //var insertQuery = $"INSERT INTO Appointments (usernameInstructor, idTimeSlot, slotDate, slotTime) VALUES('{instructor}', {slotId}, '{formatToSQLDate(myDate)}', '{myTime}')";
        //            var insertQuery = $"INSERT INTO Assignments (usernameInstructor, idAppointments) VALUES('{instructor}',{AppointmentsId})";
        //            SQL.executeQuery(insertQuery);
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
        //            return;
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
        //        return;
        //    }
        //}

        //It deletes the schedule
        private void deleteInstructorSchedule(string instructor, int slotId, DateTime myDate, DateTime myTime)
        {
            //It deletes first the Assignment
            try
            {
                var deleteQuery = $"DELETE FROM Appointments WHERE usernameInstructor='{instructor}' AND idTimeSlot={slotId} AND slotDate='{ControlFunctions.formatToSQLDate(myDate)}' AND slotTime='{myTime}'";
                SQL.executeQuery(deleteQuery);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
        }

        ////It deletes the schedule for the 2nd configuration
        //private void deleteInstructorSchedule(string instructor, int slotId, DateTime myDate, DateTime myTime)
        //{
        //    //It makes a query to obtain the Appointment id from the Appointment
        //    int AppointmentsId = obtainAppointmentId(slotId, myDate, myTime);
        //    if (AppointmentsId != 0)
        //    {
        //        //It deletes first the Assignment
        //        try
        //        {
        //            //var deleteQuery = $"DELETE FROM Appointments WHERE usernameInstructor='{instructor}' AND idTimeSlot={slotId} AND slotDate='{formatToSQLDate(myDate)}' AND slotTime='{myTime}'";
        //            var deleteQuery = $"DELETE FROM Assignments WHERE usernameInstructor='{instructor}' AND idAppointments={AppointmentsId}";
        //            SQL.executeQuery(deleteQuery);
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
        //            return;
        //        }
        //        //It deletes then the Appointment
        //        try
        //        {
        //            //var deleteQuery = $"DELETE FROM Appointments WHERE usernameInstructor='{instructor}' AND idTimeSlot={slotId} AND slotDate='{formatToSQLDate(myDate)}' AND slotTime='{myTime}'";
        //            var deleteQuery = $"DELETE FROM Appointments WHERE id={AppointmentsId}";
        //            SQL.executeQuery(deleteQuery);
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
        //            return;
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
        //        return;
        //    }
        //}

        //It makes a query to obtain the Appointment id from the Appointment for the 2nd configuration
        private int obtainAppointmentId(int slotId, DateTime myDate, DateTime myTime)
        {
            string idAppointmentsQuery = "SELECT id FROM Appointments WHERE idTimeSlot= " + slotId + " AND slotDate = '" + ControlFunctions.formatToSQLDate(myDate) + "' AND slotTime= '" + myTime + "'";
            //It gets data from database
            SQL.selectQuery(idAppointmentsQuery);
            if (SQL.read.HasRows)
            {
                SQL.read.Read();
                return int.Parse(SQL.read[0].ToString());
            }
            else return 0;
        }

        //It removes the checkboxes of the Form
        private void removeCheckBoxes()
        {
            int i = 0;
            //It cannot use foreach
            for (i= this.Controls.Count-1; i>=0; i--)
            {
                if (Controls[i] is CheckBox)
                {
                    Controls.Remove(Controls[i]);
                }
            }
        }
    }
}
