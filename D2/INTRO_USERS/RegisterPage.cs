﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    //Name: Alfredo Puche Lozoya
    //Student ID: 10009723

    public partial class RegisterPage : Form
    {
        public RegisterPage()
        {
            InitializeComponent();
            //It prepares and limites the different text controls
            textBoxUserName.MaxLength = 32;
            textBoxPassword.PasswordChar = '*';
            textBoxPassword.MaxLength = 16;
            textBoxPassword2.PasswordChar = '*';
            textBoxPassword2.MaxLength = 16;
            textBoxFirst.MaxLength = 24;
            textBoxLast.MaxLength = 24;
            textBoxEmail.MaxLength = 64;
            textBoxPhone.MaxLength = 10;
            //It loads the Client Type Combobox
            string clientTypeQuery = "SELECT name FROM ClientTypes";
            SQL.editComboBoxItems(comboBoxClientType, clientTypeQuery);
        }

        /// <summary>
        /// Registers the new user as long as all textboxes hold text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegister_Click(object sender, EventArgs e)
        {
            //variables to be used
            string username = "", password = "", password2 = "", firstname = "", lastname = "", email = "", clientType = "";
            Int32 phone = 0;

            //Check that the text boxes has something typed in it using a method
            bool hasText = checkTextBoxes();
            if (!hasText)
            {
                MessageBox.Show("Please make sure all fields have data.");
                textBoxUserName.Focus();
                return;
            }
            //It checks that the username doesn't exist yet in the DB
            bool userOK = checkUsername(textBoxUserName.Text.Trim());
            if (!userOK)
            {
                MessageBox.Show("This user already exists in the Data Base, please try another one.");
                textBoxUserName.BackColor = Color.LightCoral;
                textBoxUserName.Focus();
                return;
            }
            //It checks that the password==password2
            bool passOK = checkPassword(textBoxPassword.Text.Trim(), textBoxPassword2.Text.Trim());
            if (!passOK)
            {
                MessageBox.Show("Please make sure the password in both password fields is the same.");
                textBoxPassword.BackColor = Color.LightCoral;
                textBoxPassword2.BackColor = Color.LightCoral;
                textBoxPassword.Focus();
                return;
            }
            //It checks that the email is a proper one
            bool emailOK = checkEmail(textBoxEmail.Text.Trim());
            if (!emailOK)
            {
                MessageBox.Show("Please insert a proper email in the field.");
                textBoxEmail.BackColor = Color.LightCoral;
                textBoxEmail.Focus();
                return;
            }
            //It checks that the phone number introduced is numbers
            bool phoneOK = int.TryParse(textBoxPhone.Text.Trim(), out phone);
            if (!phoneOK)
            {
                MessageBox.Show("You can only write numbers in the phone field.");
                textBoxPhone.BackColor = Color.LightCoral;
                textBoxPhone.Focus();
                return;
            }

            //(1) GET the data from the textboxes and store into variables created above, good to put in a try catch with error message
            try
            {
                username = textBoxUserName.Text.Trim();
                password = textBoxPassword.Text.Trim();
                password2 = textBoxPassword2.Text.Trim();
                firstname = textBoxFirst.Text.Trim();
                lastname = textBoxLast.Text.Trim();
                email = textBoxEmail.Text.Trim();
                phone = int.Parse(textBoxPhone.Text.Trim());
                clientType = comboBoxClientType.Text.Trim();
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Please make sure your data is in correct format.");
                return;
            }

            //(2) Execute the INSERT statement, making sure all quotes and commas are in the correct places.
            //      Practice first on SQL Server Management Studio to make sure it is entering the correct data and in the correct format,
            //      then copy across the statement and where there are string replace the actual text for the variables stored above.
            //Example query: " INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1') "
            try
            {
                var insertClientQuery = $"INSERT INTO Clients VALUES('{username}', '{password}', '{firstname}', '{lastname}', '{email}', {phone}, '{clientType}')";
                SQL.executeQuery(insertClientQuery);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }



            //success message for the user to know it worked
            MessageBox.Show("Successfully Registered: " + firstname + " " + lastname + ". Your username is: " + username);

            //Go back to the login page since we registered successfully to let the user log in
            Hide();                                 //hides the register form
            LoginPage login = new LoginPage();      //creates the login page as an object
            login.ShowDialog();                     //shows the new login page form
            this.Close();                           //closes the register form that was hidden
        }

        /// <summary>
        /// Changes the color of the controls to white when clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxUserName_Click(object sender, EventArgs e)
        {
            textBoxUserName.BackColor = Color.White;
        }

        private void textBoxPassword_Click(object sender, EventArgs e)
        {
            textBoxPassword.BackColor = Color.White;
        }

        private void textBoxPassword2_Click(object sender, EventArgs e)
        {
            textBoxPassword2.BackColor = Color.White;
        }

        private void textBoxFirst_Click(object sender, EventArgs e)
        {
            textBoxFirst.BackColor = Color.White;
        }

        private void textBoxLast_Click(object sender, EventArgs e)
        {
            textBoxLast.BackColor = Color.White;
        }

        private void textBoxEmail_Click(object sender, EventArgs e)
        {
            textBoxEmail.BackColor = Color.White;
        }

        private void textBoxPhone_Click(object sender, EventArgs e)
        {
            textBoxPhone.BackColor = Color.White;
        }

        private void comboBoxClientType_Click(object sender, EventArgs e)
        {
            comboBoxClientType.BackColor = Color.White;
        }

        /// <summary>
        /// Clears all text boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClear_Click(object sender, EventArgs e)
        {
            initialiseTextControls();
        }

        /// <summary>
        /// Takes us back to the login screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            //hides this form currently on
            Hide();
            //is the login page as a new object               
            LoginPage login = new LoginPage();
            //Shows the login page window
            login.ShowDialog();
            //closes the current open windows so its only the new one showing
            this.Close();
        }

        /// <summary>
        /// It checks if they username doesn't exist in the DB already
        /// </summary>
        /// <returns>TRUE if the username is new and FALSE if already exists in the DB</returns>
        private bool checkUsername(string username)
        {
            bool usernameOK = true;
            //It loads the Client Type Combobox
            string usernameQuery = "SELECT username FROM Clients";
            //gets data from database
            SQL.selectQuery(usernameQuery);
            //Check that there is something to write brah
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()))
                    {
                        usernameOK = false;
                        break;
                    }
                }
            }
            return usernameOK;
        }

        /// <summary>
        /// It checks if they password textboxes have the same password
        /// </summary>
        /// <returns>TRUE if the password is the same in both textboxes, but FALSE if one is different</returns>
        private bool checkPassword(string pass1, string pass2)
        {
            bool passwordOK = true;
            if (pass1 != pass2)
            {
                passwordOK = false;
            }
            //returns true or false based on if data is in the password text boxes are the same
            return passwordOK;
        }

        /// <summary>
        /// It checks if they password textboxes have the same password
        /// </summary>
        /// <returns>TRUE if the password is the same in both textboxes, but FALSE if one is different</returns>
        private bool checkEmail(string email)
        {
            bool emailOK = true;
            if (email.IndexOf("@") == -1 || email.IndexOf("@") == 0 || email.IndexOf("@") == email.Length-1)
            {
                emailOK = false;
            }
            //returns true or false based on if data is in the password text boxes are the same
            return emailOK;
        }

        /// <summary>
        /// Checks if they text controls have data in them
        /// </summary>
        /// <returns>TRUE if all hold text, but FALSE if at least one does not hold data</returns>
        private bool checkTextBoxes()
        {
            bool holdsData = true;
            //go through all of the controls
            foreach (Control c in this.Controls)
            {
                //if its a textbox or a combobox
                if (c is TextBox || c is ComboBox)
                {
                    //If it is not the case that it is empty
                    //if ("".Equals((c as TextBox).Text.Trim()))
                    if ("".Equals(c.Text.Trim()))
                    {
                        //set boolean to false because on control is empty
                        holdsData = false;
                        c.BackColor = Color.LightCoral;
                    }
                    else
                    {
                        c.BackColor = Color.White;
                    }
                }
            }
            //returns true or false based on if data is in all text boxes or not
            return holdsData;
        }

        /// <summary>
        /// Initialises all textboxes to blank text
        /// Does nothing in terms of re-focusing
        /// </summary>
        private void initialiseTextControls()
        {
            //goes through and clears all of the textboxes
            foreach (Control c in this.Controls)
            {
                //if the it is a textbox
                if (c is TextBox)
                {
                    //clear the text box
                    (c as TextBox).Clear();
                    c.BackColor = Color.White;
                }
                else if (c is ComboBox)
                {
                    //clear the text box
                    (c as ComboBox).SelectedIndex = -1;
                    c.BackColor = Color.White;
                }
            }
            //focus on first text box
            textBoxUserName.Focus();
        }
    }
}
