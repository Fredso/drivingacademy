﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace INTRO_USERS
{
    public partial class CarAssignationPage : Form
    {
        public CarAssignationPage()
        {
            InitializeComponent();
            //It loads data into the combos
            SQL.editComboBoxItems(comboBoxInstructor, "SELECT username FROM Instructors");
            //SQL.editComboBoxItems(comboBoxCar, "SELECT license FROM Cars");
            SQL.editComboBoxItems(comboBoxCar, $"SELECT license FROM Cars WHERE license NOT IN (SELECT DISTINCT carLicense FROM Appointments WHERE slotDate >= '{ControlFunctions.formatToSQLDate(dateTimePicker1.Value)}' AND slotDate <= '{ControlFunctions.formatToSQLDate(dateTimePicker2.Value)}')");
        }


        private void comboBoxInstructor_SelectedValueChanged(object sender, EventArgs e)
        {
            cleanListViewReport();
            createListViewReport();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            cleanListViewReport();
            createListViewReport();
            actualizeCarComboBox();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            cleanListViewReport();
            createListViewReport();
            actualizeCarComboBox();
        }

        private void buttonSaveCarAssignation_Click(object sender, EventArgs e)
        {
            //It looks for the records for the instructor and the dates between the range and assings the car selected
            try
            {
                var updateQuery = $"UPDATE Appointments SET carLicense='{comboBoxCar.Text}' WHERE usernameInstructor='{comboBoxInstructor.Text}' AND slotDate>= '{ControlFunctions.formatToSQLDate(dateTimePicker1.Value)}' AND slotDate<= '{ControlFunctions.formatToSQLDate(dateTimePicker2.Value)}'";
                SQL.executeQuery(updateQuery);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
        }

        private void actualizeCarComboBox()
        {
            SQL.editComboBoxItems(comboBoxCar, $"SELECT license FROM Cars WHERE license NOT IN (SELECT DISTINCT carLicense FROM Appointments WHERE slotDate >= '{ControlFunctions.formatToSQLDate(dateTimePicker1.Value)}' AND slotDate <= '{ControlFunctions.formatToSQLDate(dateTimePicker2.Value)}' AND carLicense<>'null')");
        }

        //It clears columns and items from the listview
        private void cleanListViewReport()
        {
            listView1.Columns.Clear();
            listView1.Items.Clear();
        }


        private void createListViewReport()
        {
            string instructor = comboBoxInstructor.Text;
            //It creates the columns as per dates selected
            listView1.Columns.Add("Time");
            DateTime myDate1 = dateTimePicker1.Value;
            DateTime myDate2 = dateTimePicker2.Value;
            for (int i = 0; i<= Math.Round((myDate2.Date-myDate1.Date).TotalDays,0); i++)
            {
                listView1.Columns.Add(myDate1.AddDays(i).Date.ToString("d"));
            }
            //listView1.Refresh();
            //

            //It creates the rows as per slot times
            string slotTimesQuery = "SELECT DISTINCT slotTime FROM TimeSlots WHERE slotDate= '" + ControlFunctions.formatToSQLDate(myDate1) + "'";
            //It gets data from database
            SQL.selectQuery(slotTimesQuery);
            //It checks that there is something to write
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    ListViewItem lvi = new ListViewItem(SQL.read[0].ToString());
                    listView1.Items.Add(lvi);
                }
            }

            //It gets the registers of assignements for the instructor and the dates
            string[][] slots = new string[listView1.Items.Count][];
            for (int j = 0; j < listView1.Items.Count; j++)
            {
                slots[j] = new string[listView1.Columns.Count];
            }
            string slotMatrixQuery = "SELECT slotDate, slotTime FROM Appointments WHERE usernameInstructor= '" + instructor + "' AND slotDate >= '" + ControlFunctions.formatToSQLDate(myDate1) + "' AND slotDate <= '" + ControlFunctions.formatToSQLDate(myDate2) + "'";
            //It gets data from database
            SQL.selectQuery(slotMatrixQuery);
            //It checks that there is something to write
            if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        //It compares the name of the listview column(date) with the date of the slot coming from the query
                        for (int j=1;j<listView1.Columns.Count;j++)
                    {
                        if (((DateTime)SQL.read[0]).Date.ToString("d") == listView1.Columns[j].Text.ToString())
                        {
                            //It compares the name of the listview item(time) with the time of the slot coming from the query
                            for (int i = 0; i < listView1.Items.Count; i++)
                            {
                                if (SQL.read[1].ToString() == listView1.Items[i].Text.ToString())
                                {
                                    slots[i][j - 1] = "yes";
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                //It changes nulls by "no" in each array
                for (int i = 0; i < slots.Length; i++)
                {
                    for (int j = 0; j < slots[i].Length; j++)
                    {
                        if (slots[i][j]==null)
                        {
                            slots[i][j] = "no sched";
                        }
                        else
                        {
                            string carAssignedQuery = $"SELECT DISTINCT carLicense FROM Appointments WHERE usernameInstructor = '{instructor}' AND slotDate = '{ControlFunctions.formatToSQLDate(DateTime.Parse(listView1.Columns[j+1].Text))}'";
                            SQL.selectQuery(carAssignedQuery);
                            SQL.read.Read();
                            if (SQL.read[0].ToString() == "")
                            {
                                slots[i][j] = "to assign";
                            }
                            else
                            {
                                slots[i][j] = SQL.read[0].ToString();
                            }
                        }
                    }
                }
                //It copies the arrays to the listview items
                for (int j = 0; j < listView1.Items.Count; j++)
                {
                    listView1.Items[j].SubItems.AddRange(slots[j]);
                }
            }
            //listView1.Refresh();
        }


        private List<int> generateWeeks()
        {
            List<int> weeksList = new List<int>();
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            DateTime todayDate = DateTime.Now;
            Calendar cal = dfi.Calendar;
            int currentWeek = cal.GetWeekOfYear(todayDate, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);


            return weeksList;
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            MessageBox.Show("You clicked the column.");
        }

        private void listView1_MouseDown_1(object sender, MouseEventArgs e)
        {
            var info = listView1.HitTest(e.X, e.Y);
            var row = info.Item.Index;
            var col = info.Item.SubItems.IndexOf(info.SubItem);
            var value = info.Item.SubItems[col].Text;
            MessageBox.Show(string.Format("R{0}:C{1} val '{2}'", row, col, value));
        }

        private void listView1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            MessageBox.Show("You clicked the row.");
        }
    }
}
